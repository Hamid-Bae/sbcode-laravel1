<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/kirim" method="post">
        @csrf
        <p>First Name :</p>
        <input type="text" id="firstName" name="first-name">

        <p for="lastName">Last Name :</p>
        <input type="text" id="lastName" name="last-name">

        <p>Gender :</p>
        <div class="row">
            <input type="radio" name="radio">
            <label class="container">Male</label>
        </div>
        <div class="row">
            <input type="radio" name="radio">
            <label class="container">Female</label>
        </div>

        <p>Nationality :</p>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="singapura">Singapura</option>
            <option value="malaysia">Malaysia</option>
            <option value="australia">Australia</option>
        </select>

        <p>Langage Spoken :</p>
        <div class="row">
            <input type="checkbox" name="language">
            <label>Bahasa Indonesia</label>
        </div>
        <div class="row">
            <input type="checkbox" name="language">
            <label>English</label>
        </div>
        <div class="row">
            <input type="checkbox" name="language">
            <label>Other</label>
        </div>

        <div class="row">
            <p>Bio :</p>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        </div>

        <button type="submit" value="sign up">sign up</button>
    </form>
</body>
</html>