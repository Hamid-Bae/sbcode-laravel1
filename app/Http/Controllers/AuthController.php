<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome() {
        return view("welcome");
    }
    public function register() {
        return view("register");
    }
    public function kirim(Request $request) {
        $firstName = $request->input('first-name');
        $lastName = $request->input('last-name');
        return view("welcome", compact("firstName", "lastName"));
        // dd($firstName);
        // dd($lastName);
    }
}
